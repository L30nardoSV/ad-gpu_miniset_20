# Miniset of 20 protein-ligand inputs for AutoDock-GPU

This is a subset of the full set available at https://zenodo.org/record/4031961#.YVdeDVTP3Qp.

## Characteristics

| ID                    | Nrot   | Natom |
| :-------------------: | -----: | -----:|
| [1u4d](./data/1u4d)   |      0 |    23 | 
| [1xoz](./data/1xoz)   |      1 |    30 |
| [1yv3](./data/1yv3)   |      2 |    23 | 
| [1owe](./data/1owe)   |      3 |    27 |
| [1oyt](./data/1oyt)   |      4 |    34 | 
| [1ywr](./data/1ywr)   |      5 |    38 |
| [1t46](./data/1t46)   |      6 |    40 | 
| [2bm2](./data/2bm2)   |      7 |    33 |
| [1mzc](./data/1mzc)   |      8 |    38 | 
| [1r55](./data/1r55)   |      9 |    27 |


| ID                    | Nrot   | Natom |
| :-------------------: | -----: | -----:|
| [5wlo](./data/5wlo)   |     10 |    46 | 
| [1kzk](./data/1kzk)   |     11 |    45 |
| [3s8o](./data/3s8o)   |     12 |    44 | 
| [5kao](./data/5kao)   |     15 |    44 |
| [1hfs](./data/1hfs)   |     18 |    54 | 
| [1jyq](./data/1jyq)   |     20 |    60 |
| [2d1o](./data/2d1o)   |     23 |    44 | 
| [3drf](./data/3drf)   |     26 |    63 |
| [4er4](./data/4er4)   |     30 |    93 | 
| [3er5](./data/3er5)   |     31 |   108 |

## License

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg



